function showFireworks() {
    var container = $(".fireworks-container");

    for (var i = 0; i < 50; i++) {
        var firework = $('<div class="firework"></div>');
        var positionX = Math.random() * window.innerWidth;
        var positionY = Math.random() * window.innerHeight;
        var size = Math.random() * 20 + 5;
        var delay = Math.random() * 2 + 0.1;

        firework.css({
            left: positionX,
            top: positionY,
            width: size,
            height: size,
        });

        container.append(firework);

        firework.delay(delay * 1000).fadeOut(300, function () {
            $(this).remove();
        });
    }

    container.fadeIn(200).delay(2000).fadeOut(200);
}